<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Circuler_controller extends CI_Controller {

   

    public function __construct() {
        parent::__construct();
    //   $this->load->helper(array('form', 'url'));
        $this->load->model('Circuler_model');
        $this->load->helper('url');
   }
function viewcirculer($sort_type='desc',$sort_on='id') {    
    $this->load->library('search');
$search_data = $this->search->data_search();
$this->session->set_userdata('search_data', json_encode($search_data));
$status_options =array('Active'=>'Active','Inactive'=>'Inactive');
$this->assign('status_option',$status_options);
       $data['view']= $this->Circuler_model->searchproduct();
       $this->load->view('circuler/view_circuler',$data);
//       echo '<pre>';
//        print_r($data);exit;
    }

    function display(){
    $this->load->view('display/display_file');
    }
            
    
    function new_circuler(){
        
        $this->load->view('circuler/new_circuler_view');
        
    }
    
     function new_circuler_post(){
       $data=array();
      
     $data['circuler_type'] = $this->input->post('type',TRUE);
     $data['circuler_title']= $this->input->post('title',true);
     $data['circuler_descr']= $this->input->post('description',true);
     $data['experience']= $this->input->post('experience',true);
     $data['date']= $this->input->post('application_date',true);
//     $data['upload_file']= $this->input->post('user_file',true);
     
         $result = $this->do_upload('upload_file');
         // print_r($result);exit();
         if($result['upload_data']){
             $data['upload_file']=$result['upload_data']['file_name'];
         }
      $this->Circuler_model->get_circuler($data);
  
     redirect('index.php/Circuler_controller/new_circuler');
     
     }

public function do_upload(){
    $config['upload_path']="./project/upload/";
    $config['allowed_types']='*';
    $config['max_width']='1024';
    $config['max_height']='786';
    $this->load->library('upload',$config);
if(!$this->upload->do_upload('upload_file')){
$error =array('error' => $this->upload->display_errors(),'upload_data' => '');
return $error;
} else {
    $data = array('upload_data' => $this->upload->data(),'error' =>'');
    return $data;
}

}
public function applied_post(){
    $this->load->view('circuler/applied');
}
public function viewuserinfo(){
    $this->load->view('circuler/userinfo');
}
public function cv(){
    $data=array();          
     $data['circuler_title']= $this->input->post('title',true);
     $data['contact']= $this->input->post('contact',true); 
     $data['ssc']= $this->input->post('ssc',true); 
     $data['hsc']= $this->input->post('hsc',true); 
     $data['bsc']= $this->input->post('bsc',true); 
     $data['msc']= $this->input->post('msc',true); 
     $data['experience']= $this->input->post('experience',true);
     $data['date']= $this->input->post('application_date',true); 
     $result= $this->cv_upload('upload_file');
//print_r($result);exit();
     if($result['upload_data']){
         $data['upload_file']=$result['upload_data']['file_name'];
     }
     $this->Circuler_model->get_cv($data);
     redirect('index.php/Circuler_controller/applied_post');

}
public function cv_upload(){
  $config['upload_path']="./project/upload/upload_cv/";  
  $config['allowed_types']='*';
  $config['max_width']='1024';
  $config['max_height']='786';
  $this->load->library('upload',$config);
if(!$this->upload->do_upload('upload_file')){
$error=array('error'=> $this->upload->display_errors(),'upload_data'=>'');
return $error;
}else{
    $data=array('upload_data'=> $this->upload->data(),'error'=>'');
    return $data;
}
}
        
}
