<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require_once APPPATH.'PHPExcel/Classes/PHPExcel/IOFactory.php';

class Result_controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
                    
                $this->load->library('excel','form_validation');//load PHPExcel library 
		//$this->load->model('upload_model');//To Upload file in a directory
                $this->load->model('Result_model');
    }
    public function viewresult(){
        $this->load->view('result/result_view');
    }
    
     public function uploadresult(){
         
        $this->load->view('result/upload_result');
    }
    
	           
 public function saveresult(){

        $configUpload['upload_path'] = FCPATH.'/project/upload/result/';
        $configUpload['allowed_types'] = 'xls|xlsx|csv';
        $configUpload['max_size'] = '1048576';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('upload_file');
        $upload_data = $this->upload->data();
        //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];
        $objReader= PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $objPHPExcel=$objReader->load(FCPATH.'/project/upload/result/'.$file_name);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $objPHPExcel->getSheet(0);
        
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $drop_query = "DROP TABLE IF EXISTS `result_tbl`";

        // First you have to ensure drop table brefore creating it.
        $this->db->query($drop_query);

        $create_query = "CREATE TABLE `result_tbl`(\n";
        $create_query .= " id INT ( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,\n";

        $columns  = array();
	$values = array();

        foreach($sheet->getRowIterator() as $rowIndex => $row) {
	    // Need to check if this call is working. You should get the full row data here.
	    $rowData = $sheet->rangeToArray('A'.$rowIndex.':'.$highestColumn.$rowIndex, NULL, TRUE, FALSE);

            if ($rowIndex == 1) {
		$column_statements = array();
                foreach ($rowData[0] as $key => $name) {
                $column_statements[]= " $name VARCHAR( 100 ) NOT NULL";
		    // Keep the columns in an array to insert data later with it.
                    $columns[]= $name;
                }
                // Closing the statement
                $create_query .= implode(',', $column_statements) . ');';
		 
                // Creating table with first row information
              $this->db->query($create_query);
                

            } else {
                // Preparing the insert query string except the first row
		// result : ('Another title', 'Another name', 'Another date')
                $values[] =  "('" . implode("','", $rowData[0]) . "')";
                
            }
        }
	// Now we have both and column data
	// Preparing bulk insert query with columns and values information
	// Output : INSERT INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date'), ('Another title', 'Another name', 'Another date')
	$query = "Insert into `result_tbl` (".implode(',', $columns) . ") VALUES ".implode(',', $values). ";";


        $this->Result_model->add_result( $query );
        redirect('index.php/Result_controller/uploadresult');

    }
}

