<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!--<link href="style.css" rel="stylesheet" /> -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
      body{
          background-color: #333;
      } 
  .modal-header, h4, .close {
      background-color: #5cb85c;
      color:white !important;
      text-align: center;
      font-size: 30px;
  }
  .modal-footer {
      background-color: #f9f9f9;
  }
  </style>
</head>
<body>
   
<div class="container">
  <br><br><br>
  <!-- Trigger the modal with a button -->

  <button type="button" class="nav navbar-nav navbar-right" id="myBtn"><span class="glyphicons glyphicons-login-in"></span>Login</button>
 
  <!--btn btn-default btn-lg -->
  
   
  <!-- Modal -->
  
  
  
  
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Login</h4>
          <h3>
            <?php $messages=$this->session->userdata('messages');
            if(isset($messages))
            {
                echo $messages;
            $this->session->unset_userdata('messages');
                        
            }                       
            ?>                                   
          </h3>
        </div>

        <div class="modal-body" style="padding:40px 50px;">
            <form name="user login information" role="form" method="post" action="<?php echo base_url();?>index.php/Login_controller/login_view" enctype="multipart/form-data">
            <div class="form-group">
              <label for="email"><span class="glyphicon glyphicon-user"></span> UserEmail</label>
              <input type="text" class="form-control" name="emailaddress" id="email" placeholder="Enter email" required="">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
              <input type="password" class="form-control" name="password" id="psw" placeholder="Enter password" required="">
            </div>
                 
            <div class="checkbox">
                <label><input type="checkbox" value="" >Remember me</label>
            </div>
              <button type="submit" class="btn btn-success btn-block "><span class="glyphicons glyphicons-log-in"></span> Login</button>
          </form>
            
        </div>
        
    
        <div class="modal-footer">
           
            
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                  <p>Not a member? 
                      <button type="submit" class="navar navar-success navar-right"  id="myBtn1" data-dismiss="modal">Sign Up</button>
            <!--  <a id="myBtn1" href="#">Sign Up</a></p></button>-->
          <p>Forgot <a href=" ">Password?</a></p>
        </div>
      </div>
      
    </div>
  </div> 
  
   
  
   <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Sign Up</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
            <form name="signup information" role="form" method="post" action="<?php echo base_url();?>index.php/Login_controller/sign_view">
              <div class="form-group">
              <label for="firstname"><span class="glyphicon glyphicon-user"></span> First Name</label>
              <input type="text" class="form-control" name="firstname" id="usrname" placeholder="Enter firstname" required="">
            </div>
              <div class="form-group">
              <label for="lastname"><span class="glyphicon glyphicon-user"></span>Last Name</label>
              <input type="text" class="form-control" name="lastname" id="usrname" placeholder="Enter lastname" required="">
            </div>
                <div class="form-group">
              <label for="address"><span class="glyphicon glyphicon-user"></span>Address</label>
              <input type="text" class="form-control" name="address" id="usrname" placeholder="Enter address" required="">
            </div>
            <div class="form-group">
              <label for="emailaddress"><span class="glyphicon glyphicon-user"></span> Email Address</label>
              <input type="text" class="form-control" name="emailaddress" id="usrname" placeholder="Enter email" required="">
            </div>
            <div class="form-group">
              <label for="password"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
              <input type="password" class="form-control" name="password" id="psw" placeholder="Enter password" required="">
            </div>
                
            <div class="checkbox">
                <label><input type="checkbox" value="check" name="remember" checked>Remember me</label>
            </div>
              <div class="form-group">
                  <label class="col-md-2 control-label"> </label>
                  <div class="col-md-4">
                      <button class="btn btn-success" type="submit" name="submit" value="submit">submit</button>
                  </div>
            </div>
              <!-- <button type="submit" class="btn btn-success btn-block"><span class="glyphicons glyphicons-log-in"></span> Login</button>-->
          </form>
        </div>
    </div>
       
      </div>
      
    </div>

    </div>
  
   
    
<script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
  
    $("#myBtn1").click(function(){
        $("#myModal1").modal();
    });
});

</script>

</body>
</html>

