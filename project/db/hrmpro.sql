-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2017 at 06:07 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrmpro`
--

-- --------------------------------------------------------

--
-- Table structure for table `circuler_tbl`
--

CREATE TABLE `circuler_tbl` (
  `circuler_id` int(11) NOT NULL,
  `circuler_type` varchar(200) NOT NULL,
  `circuler_title` varchar(200) NOT NULL,
  `circuler_descr` varchar(200) NOT NULL,
  `experience` year(2) NOT NULL,
  `date` date NOT NULL,
  `upload_file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `circuler_tbl`
--

INSERT INTO `circuler_tbl` (`circuler_id`, `circuler_type`, `circuler_title`, `circuler_descr`, `experience`, `date`, `upload_file`) VALUES
(1, 'sddfas', 'sdfaf', 'sdfasf', 03, '2017-10-03', 'project/upload/Jewel_Rana_cover_letter.doc'),
(2, '', '', '', 00, '0000-00-00', 'Jewel_Rana_cover_letter1.doc');

-- --------------------------------------------------------

--
-- Table structure for table `cv_tbl`
--

CREATE TABLE `cv_tbl` (
  `cv_id` int(11) NOT NULL,
  `circuler_title` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `ssc` varchar(200) NOT NULL,
  `hsc` varchar(200) NOT NULL,
  `bsc` varchar(200) NOT NULL,
  `msc` varchar(200) NOT NULL,
  `experience` year(2) NOT NULL,
  `date` date NOT NULL,
  `upload_file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cv_tbl`
--

INSERT INTO `cv_tbl` (`cv_id`, `circuler_title`, `contact`, `ssc`, `hsc`, `bsc`, `msc`, `experience`, `date`, `upload_file`) VALUES
(1, 'safsa', 'sdfsd', 'sdfas', 'sdfsadf', 'sdfsad', 'sdfsd', 03, '2017-10-12', 'Jewel_Rana_cover_letter.doc');

-- --------------------------------------------------------

--
-- Table structure for table `regi_tbl`
--

CREATE TABLE `regi_tbl` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `regi_tbl`
--

INSERT INTO `regi_tbl` (`user_id`, `first_name`, `last_name`, `address`, `email`, `password`) VALUES
(1, 'shahjalal', 'islam', 'dhaka', 'fmshahjalal@gmail.com', '202cb962ac59075b964b07152d234b70'),
(2, 'kabir', 'islam', 'badda', 'kabir@gmail.com', '202cb962ac59075b964b07152d234b70'),
(3, 'sadfas', 'sdfas', 'sadffa', 'aziz@gmail.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Table structure for table `result_tbl`
--

CREATE TABLE `result_tbl` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `result_tbl`
--

INSERT INTO `result_tbl` (`id`, `name`, `status`, `email`, `mobile`, `address`) VALUES
(1, 'sdSAD', 'asddf', 'sdd@gmail.com', '234', 'fdfsd'),
(2, 'sfasd', 'active', 'sadf@gmail.com', '123', 'dhaka'),
(3, 'sdfaf', 'sada', 'sad@gmail.com', '123', 'df');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `circuler_tbl`
--
ALTER TABLE `circuler_tbl`
  ADD PRIMARY KEY (`circuler_id`);

--
-- Indexes for table `cv_tbl`
--
ALTER TABLE `cv_tbl`
  ADD PRIMARY KEY (`cv_id`);

--
-- Indexes for table `regi_tbl`
--
ALTER TABLE `regi_tbl`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `result_tbl`
--
ALTER TABLE `result_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `circuler_tbl`
--
ALTER TABLE `circuler_tbl`
  MODIFY `circuler_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cv_tbl`
--
ALTER TABLE `cv_tbl`
  MODIFY `cv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `regi_tbl`
--
ALTER TABLE `regi_tbl`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `result_tbl`
--
ALTER TABLE `result_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
